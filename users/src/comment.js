const mongoose = require('mongoose');
const { COMMENT_COLLECTION_NAME, USER_COLLECTION_NAME  } = require('./consts');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
  content: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: USER_COLLECTION_NAME
  }
});

const Comment = mongoose.model(COMMENT_COLLECTION_NAME, CommentSchema);

module.exports = {
  CommentSchema,
  Comment
}