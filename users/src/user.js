const mongoose = require('mongoose');
const { PostSchema } = require('./post');
const { BLOGPOST_COLLECTION_NAME, USER_COLLECTION_NAME  } = require('./consts');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    validate: {
      validator: (name) => name.length > 2,
      message: 'Name must be longer than 2 characters.'
    },
    required: [true, 'Name is required.']
  },
  posts: [PostSchema],
  likes: Number,
  blogPosts: [{
    type: Schema.Types.ObjectId,
    ref: BLOGPOST_COLLECTION_NAME
  }]

});

UserSchema.virtual('postCount').get(function() {
  return this.posts.length;
});

UserSchema.pre('remove', function(next) {
  const BlogPost = mongoose.model(BLOGPOST_COLLECTION_NAME);

  BlogPost.deleteMany({ _id: { $in: this.blogPosts } })
    .then(() => next());
});

const User = mongoose.model(USER_COLLECTION_NAME, UserSchema);

module.exports = {
  UserSchema,
  User
}