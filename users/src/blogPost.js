const mongoose = require('mongoose');
const { BLOGPOST_COLLECTION_NAME, COMMENT_COLLECTION_NAME  } = require('./consts');

const Schema = mongoose.Schema;

const BlogPostSchema = new Schema({
  title: String,
  content: String,
  comments: [{
    type: Schema.Types.ObjectId,
    ref: COMMENT_COLLECTION_NAME
  }]
});

const BlogPost = mongoose.model(BLOGPOST_COLLECTION_NAME, BlogPostSchema);

module.exports = {
  BlogPostSchema,
  BlogPost
}