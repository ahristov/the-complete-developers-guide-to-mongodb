const mongoose = require('mongoose');
const assert = require('assert');

const { BlogPost } = require('../src/blogPost');
const { User } = require('../src/user');

describe('Middleware', () => {
  let joe, blogPost;

  beforeEach((done) => {
    joe = new User({ name: 'Joe' });
    blogPost = new BlogPost({ title: 'JS is Great', content: 'Yep, it really is' });

    joe.blogPosts.push(blogPost);

    Promise.all([ joe.save(), blogPost.save() ])
      .then(() => done())
  });

  it('users delete also deletes blogposts', (done) => {
    joe.remove()
      .then(() => BlogPost.countDocuments())
      .then((count) => {
        assert(count === 0);
        done();
      });
  });

});