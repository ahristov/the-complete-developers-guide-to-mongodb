const assert = require('assert');
const { User } = require('../src/user');

describe('Updating documents', () => {
  let joe;

  beforeEach((done) => {
    joe = new User({ name: 'Joe', postCount: 0 });
    joe.save()
      .then(() => done());
  });

  function assertName(operation, done) {
    operation
    .then(() => User.find({}))
    .then((users) => {
      assert(users.length === 1);
      assert(users[0].name === 'Alex');
      done();
    });
  }

  it('model instance set and save', (done) => {
    joe.set('name', 'Alex');
    assertName(joe.save(), done);
  });

  it('model instance can update', (done) => {
    assertName(joe.updateOne({ name: 'Alex' }), done);
  });

  it('class method can update', (done) => {
    assertName(
      User.updateMany({ name: 'Joe' }, { name: 'Alex' }),
      done();
    );
  });

  it('class method can update one document', (done) => {
    assertName(
      User.updateOne({ name: 'Joe' }, { name: 'Alex' }),
      done();
    );
  });

  // DeprecationWarning: Mongoose: `findOneAndUpdate()` and `findOneAndDelete()` without the `useFindAndModify` option set to false are deprecated.
  // See: https://mongoosejs.com/docs/deprecations.html#findandmodify

  // it('class method can update one by Id', (done) => {
  //   assertName(
  //     User.findByIdAndUpdate(joe._id, { name: 'Alex' }),
  //     done
  //   );
  // });

  // Update operators:
  // [Reference](https://docs.mongodb.com/manual/reference/operator/update/)
  // We do not want to fetch data, but send command to the server to update in place.
  //

  it('A user can have their postCount incremented by 1', (done) => {
    User.updateMany({ name: 'Joe' }, { $inc: { likes: 10 } })
      .then(() => User.findOne({ name: 'Joe' }))
      .then((user) => {
        assert(user.likes === 10);
        done();
      });
  });

});