const assert = require('assert');
const { User } = require('../src/user');

describe('Deleting documents', () => {
  let joe;

  beforeEach((done) => {
    joe = new User({ name: 'Joe' });
    joe.save()
      .then(() => done());
  });

  it('model instance remove', (done) => {
    joe.remove()
      .then(() => User.findOne({ name: 'Joe' }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });

  it('class method deleteOne', (done) => {
    // Delete at most one matching document
    User.deleteOne({ name: 'Joe' })
    .then(() => User.findOne({ name: 'Joe' }))
    .then((user) => {
      assert(user === null);
      done();
    });
  });

  it('class method deleteMany', (done) => {
    // Delete all the matching document
    User.deleteMany({ name: 'Joe' })
    .then(() => User.findOne({ name: 'Joe' }))
    .then((user) => {
      assert(user === null);
      done();
    });
  });

  it('class method findByIdAndDelete', (done) => {
    User.findByIdAndDelete(joe._id)
    .then(() => User.findOne({ name: 'Joe' }))
    .then((user) => {
      assert(user === null);
      done();
    });
  });

  it('class method findOneAndDelete', (done) => {
    User.findOneAndDelete({ name: 'Joe'})
      .then(() => User.findOne({ name: 'Joe' }))
      .then((user) => {
        assert(user === null);
        done();
      });
  });

});