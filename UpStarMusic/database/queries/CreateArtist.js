const { Artist } = require('../models/artist');

/**
 * Create new artist in the artists collection.
 * @param {object} artistProps - Object containing a name, age, yearsActive, and genre
 * @return {promise} A promise that resolves with the Artist that was created
 */
module.exports = (artistProps) => {
  const artist = new Artist(artistProps);
  return artist.save();
};
