const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PointSchema = new Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true
  },
  coordinates: {
    type: [Number],
    index: '2dsphere'
  }
});

const DriverSchema = new Schema({
  email: {
    type: String,
    required: [true, 'Email is required.']
  },
  driving: {
    type: Boolean,
    default: false
  },
  location: {
    type: PointSchema
  }
});

DriverSchema.index({location: '2dsphere'});

const Driver = mongoose.model('driver', DriverSchema);

module.exports = Driver;
