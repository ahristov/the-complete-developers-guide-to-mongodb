const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');

const app = require('../../app');

const Driver = mongoose.model('driver');

describe('Drivers controller', () => {

  it('GET /api/drivers returns near by drivers', (done) => {
    const seattleDriver = new Driver({
      email: 'seattle@test.com',
      location: { type: 'Point', coordinates: [-122.4759902, 47.6147628] }
    });
    const miamiDriver = new Driver({
      email: 'miami@test.com',
      location: { type: 'Point', coordinates: [-80.253, 25.791] }
    });

    Promise.all([ seattleDriver.save(), miamiDriver.save() ])
      .then(() => {
        request(app)
          .get('/api/drivers?lng=-80&lat=25')
          .end((err, response) => {
            assert(response.body.length === 1);
            assert(response.body[0].email === 'miami@test.com');
            assert(response.body[0].location.coordinates[0] === -80.253);
            assert(response.body[0].location.coordinates[1] === 25.791);
            done();
          })
      })
  });

  it('POST /api/drivers creates a new driver', (done) => {
    Driver.countDocuments().then(count => {
      request(app)
      .post('/api/drivers')
      .send({ email: 'test@tes.com' })
      .end((err, response) => {
        Driver.countDocuments().then(newCount => {
          assert(newCount === count + 1);
          done();
        });
      });
    });
  });

  it('PUT /api/drivers/:id updates the driver', (done) => {
    const driver = new Driver({ email: 't@t.com', driving: false });

    driver.save()
      .then(() => {
        request(app)
          .put(`/api/drivers/${driver._id}`)
          .send({ driving: true })
          .end(() => {
            Driver.findOne({ email: driver.email })
              .then(driver => {
                assert(driver.driving === true);
                done();
              })
          });
      })
  });

  it('DELETE /api/drivers/:id deletes the driver', (done) => {
    const driver = new Driver({ email: 't@t.com', driving: false });

    driver.save()
      .then(() => {
        request(app)
          .delete(`/api/drivers/${driver._id}`)
          .end(() => {
            Driver.findOne({ email: driver.email })
              .then(driver => {
                assert(driver === null);
                done();
              })
          });
      })
  });

});

