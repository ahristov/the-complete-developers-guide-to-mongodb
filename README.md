# The Complete Developers Guide to MongoDB

This project contains notes and code from studying [The Complete Developers Guide to MongoDB](https://www.udemy.com/course/the-complete-developers-guide-to-mongodb).

## 🏠 [Homepage](https://bitbucket.org/ahristov/the-complete-developers-guide-to-mongodb/src)

## Run users

```bash
cd users
yarn
yarn run test
```

## Run UpStarMusic

```bash
cd UpStarMusic
yarn
yarn start
```

## Run Muber

To execute the continuous tests, run:

```bash
cd muber
yarn
yarn test
```

To execute the API server, run:

```
cd muber
yarn
yarn dev
```

Example API calls:

```

POST http://localhost:3050/api/drivers HTTP/1.1
Content-Type: application/json

{
  "email": "seattle@test.com",
  "location": {
    "type": "Point",
    "coordinates": [-122.4759902, 47.6147628]
  }
}

HTTP/1.1 200 OK

{
  "driving": false,
  "_id": "5ed9c7a1efa13f99bc0f3679",
  "email": "seattle@test.com",
  "location": {
    "_id": "5ed9c7a1efa13f99bc0f367a",
    "type": "Point",
    "coordinates": [
      -122.4759902,
      47.6147628
    ]
  },
  "__v": 0
}

POST http://localhost:3050/api/drivers HTTP/1.1
Content-Type: application/json

{
  "email": "miami@test.com",
  "location": {
    "type": "Point",
    "coordinates": [-80.253, 25.791]
  }
}

HTTP/1.1 200 OK

{
  "driving": false,
  "_id": "5ed9c5dfefa13f99bc0f3674",
  "email": "miami@test.com",
  "location": {
    "_id": "5ed9c5dfefa13f99bc0f3675",
    "type": "Point",
    "coordinates": [
      -80.253,
      25.791
    ]
  },
  "__v": 0
}

GET http://localhost:3050/api/drivers?lng=-80&lat=25 HTTP/1.1


HTTP/1.1 200 OK

[
  {
    "driving": false,
    "_id": "5ed9c5dfefa13f99bc0f3674",
    "email": "miami@test.com",
    "location": {
      "_id": "5ed9c5dfefa13f99bc0f3675",
      "type": "Point",
      "coordinates": [
        -80.253,
        25.791
      ]
    },
    "__v": 0
  }
]

```


## Notes

Query and projection operators:
https://docs.mongodb.com/manual/reference/operator/query/

To _suppress one_ single test case, rename "it()" to "xit()". Example output:

```
 22 passing (772ms)
  1 pending
```

To _only run one_ single test case, rename "it()" to "it.only()".

Middleware events are "pre" and "post" in relation to an event. Event types are: "save", "validate", "remove".

Create text index:

```
> use upstar_music
switched to db upstar_music
> show collections
artists
> db.artists.createIndex({ name: "text" })
{
        "createdCollectionAutomatically" : false,
        "numIndexesBefore" : 1,
        "numIndexesAfter" : 2,
        "ok" : 1
}
```

Create text index:

```
> use upstar_music
switched to db upstar_music
> show collections
artists
> db.artists.createIndex({ name: "text" })
{
        "createdCollectionAutomatically" : false,
        "numIndexesBefore" : 1,
        "numIndexesAfter" : 2,
        "ok" : 1
}
```

Environment variables:

```

  Machine:
  -----------------------
  Environment Variable
  NODE_ENV
      |
      |
  Node App
  process.env.NODE_ENV
  -----------------------

```

For Geo support see:
[GeoJSON](https://mongoosejs.com/docs/geojson.html)
[Geo-spatial queries](http://thecodebarbarian.com/80-20-guide-to-mongodb-geospatial-queries)
[Good example](https://stackoverflow.com/questions/23188875/mongodb-unable-to-find-index-for-geonear-query)

## Bonus

Interested in some of my other courses? Try one out now!

Microservices with Node JS and React - https://www.udemy.com/course/microservices-with-node-js-and-react/?couponCode=2F03CBF880ACD340A0ED

Docker and Kubernetes: The Complete Guide - https://www.udemy.com/course/docker-and-kubernetes-the-complete-guide/?couponCode=BCE2BB4C9D64B4DEF6D9

Modern React with Redux - https://www.udemy.com/course/react-redux/?couponCode=23EAF677D868ADFF9D4B

Typescript: The Complete Developer's Guide - https://www.udemy.com/course/typescript-the-complete-developers-guide/?couponCode=95128CAA1E8E33D74D81

The Modern Angular Bootcamp - https://www.udemy.com/course/the-modern-angular-bootcamp/?couponCode=C2D257045C1B36C67B58

The Complete React Native and Redux Course - https://www.udemy.com/course/the-complete-react-native-and-redux-course/?couponCode=6CD6E9E55258CCA52058

The Coding Interview Bootcamp: Algorithms + Data Structures - https://www.udemy.com/course/coding-interview-bootcamp-algorithms-and-data-structure/?couponCode=6864A5C2C39035085EC7

Machine Learning with JavaScript - https://www.udemy.com/course/machine-learning-with-javascript/?couponCode=FC4EC1C52AB8C50556B2

Node JS: Advanced Concepts - https://www.udemy.com/course/advanced-node-for-developers/?couponCode=11D3195D75FF841F9FA0
